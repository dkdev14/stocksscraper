//function to retrieve list of companies for given industry
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var rp = require('request-promise');
var fs = require('fs');

var selectors = require('../resources/selectors');
var urls = require('../resources/urls');
var companyOverviewInstance = require('./getCompanyOverview');



module.exports = {
   getCompanyList: function (industryUrl, industryName) {

     rp({
       url: industryUrl,
       method: 'get',
       timeout: 600000
        })
       .then(function (html){

         var companyList = [];
          var companyItem;
         //load html contents in jquery selector via cheerio;
         var $ = cheerio.load(html);
         console.log("outside companies list....");
         $(selectors.INDUSTRY_CLASSIFICATION.COMPANY_LIST).each(function(i, elem) {
             companyItem = { company: "", industry: industryName, link: ""};

             var companyName = $(this).first().find(selectors.INDUSTRY_CLASSIFICATION.COMPANY_NAME).first().text();
             var companyLink = $(this).first().find(selectors.INDUSTRY_CLASSIFICATION.COMPANY_NAME).first().attr('href');

             if(companyName != undefined && companyLink != undefined){
                 companyItem.company = companyName;
                 companyItem.link = companyLink;
             }

             if(companyItem.company != "" && companyItem.link != ""){
                 companyList.push(companyItem);
             }

           });

           if(companyList.length > 0) {

            // companyOverviewInstance.getCompanyOverview(companyList[0].link, companyList[0].company, industryName);


             for(company in companyList){

               companyOverviewInstance.getCompanyOverview(companyList[company].link, companyList[company].company, industryName);
             }


            /* fs.writeFile('companies/'+ industryName +'.json', JSON.stringify(companyList, null, 4), function(err){

                 console.log('Companies File successfully written! - Check your --Companies-- directory for the .json file');

             })*/
           }
           else {
             console.error("Company list is empty. Please check the company list selectors and try again!!");
           }
       })
       .catch(function (err) {
       console.error('Error occured while requesting url - ' + industryUrl);
       console.error(err);
       return;
     });

  }
}
