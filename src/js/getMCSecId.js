//function to retrieve list of companies for given industry
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var rp = require('request-promise');
var fs = require('fs');

var selectors = require('../resources/selectors');
var urls = require('../resources/urls');
var companyOverviewInstance = require('./getCompanyOverview');



module.exports = {
   getSecurityId: function (industryUrl, industryName) {

     rp({
       url: industryUrl,
       method: 'get',
       timeout: 600000
        })
       .then(function (html){

         var companyList = [];
          var companyItem;
         //load html contents in jquery selector via cheerio;
         var $ = cheerio.load(html);

         $(selectors.INDUSTRY_CLASSIFICATION.COMPANY_LIST).each(function(i, elem) {
             companyItem = { company: "", industry: industryName, secId: "", bseCode: ""};

             var companyName = $(this).first().find(selectors.INDUSTRY_CLASSIFICATION.COMPANY_NAME).first().text();
             var companyLink = $(this).first().find(selectors.INDUSTRY_CLASSIFICATION.COMPANY_NAME).first().attr('href');
             var bseArray = $(this).first().find('td.PR').children('span.ic_tradenwicn.btn_tradep_pop.ML2');
             if(companyName != undefined && companyLink != undefined && bseArray.length > 0){
                companyItem.company = companyName;
                var id = companyLink.split("/");
                companyItem.secId = (id.length > 1)? id[id.length - 1].toLowerCase() : "";
                var bseString = bseArray.eq(0).attr('onclick').split(",");
                companyItem.bseCode = (bseString.length > 1) ? bseString[bseString.length - 1].match(/[0-9 , \.]+/g).toString() : "";
             }

             if(companyItem.company != "" && companyItem.secId != ""){
                 companyList.push(companyItem);
             }

           });

           if(companyList.length > 0) {

            //companyOverviewInstance.getCompanyOverview(companyList[0].link, companyList[0].company, industryName);


             for(company in companyList){
              console.log("***************************************");
              console.log("Name : " + companyList[company].company);
              console.log("Name : " + companyList[company].secId);
              console.log("Name : " + companyList[company].bseCode);
              console.log("Name : " + companyList[company].industry);
              console.log("***************************************");

            }


             fs.writeFile('mcSecurity/'+ industryName +'.json', JSON.stringify(companyList, null, 4), function(err){

                 console.log('Companies File successfully written! - Check your --mcSecurity-- directory for the .json file');

             })
           }
           else {
             console.error("Company list is empty. Please check the company list selectors and try again!!");
           }
       })
       .catch(function (err) {
       console.error('Error occured while requesting url - ' + industryUrl);
       console.error(err);
       return;
     });

  }
}
