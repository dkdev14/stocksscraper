//function to retrieve list of industries for given url
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var rp = require('request-promise');
var fs = require('fs');

var selectors = require('../resources/selectors');
var urls = require('../resources/urls');
var companyInstance = require('./getCompanyList');
var secInstance = require('./getMCSecId');



module.exports = {
   getIndustryList: function (url) {

     rp(url)
       .then(function (html){

         var industryList = [];
         //load html contents in jquery selector via cheerio;
         var $ = cheerio.load(html);
         $(selectors.INDUSTRY_CLASSIFICATION.INDUSTRY_LINKS).each(function(i, elem) {

             var industryItem = { industry: "", link: ""};

             var link =  $(this).find(selectors.INDUSTRY_CLASSIFICATION.INDUSTRY_NAME).first().attr('href');

             industryItem.industry = $(this).find(selectors.INDUSTRY_CLASSIFICATION.INDUSTRY_NAME).first().text();
             industryItem.link = urls.BASE_URL + link;

             if(industryItem.industry != "" && link != undefined){
                 industryList.push(industryItem);
             }
             else if(industryItem.industry == "Aluminium"){
                 industryItem.link = urls.BASE_URL + urls.INDUSTRY_CLASSIFICATION_RELATIVE_URL;
                 industryList.push(industryItem);
             }

           });


           //for each industry, get company links
           if(industryList.length > 0) {

             //companyInstance.getCompanyList(industryList[0].link, industryList[0].industry);

             for (var industry in industryList) {
              //companyInstance.getCompanyList(industryList[industry].link, industryList[industry].industry);
              secInstance.getSecurityId(industryList[industry].link, industryList[industry].industry);

             }

            /* fs.writeFile('industryList.json', JSON.stringify(industryList, null, 4), function(err){

                 console.log('File successfully written! - Check your project directory for the output.json file');

             }) */
           }
           else {
             console.error("Industry list is empty. Please check the industry list selectors and try again!!");
           }
       })
       .catch(function (err) {
       console.error('Error occured while requesting url - ' + url);
       console.error(err);
       return;
     });

  }
}
