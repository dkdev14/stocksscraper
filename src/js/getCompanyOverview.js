//function to retrieve list of companies for given industry
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var rp = require('request-promise');
var fs = require('fs');

var selectors = require('../resources/selectors');
var urls = require('../resources/urls');
var firebaseSetup = require('./firebaseSetup');

var firebase = firebaseSetup.firebaseInstance;

var companyOverviewItem = {
                       bseCode: "",
                       industry: "",
                       marketCap: "",
                       peRatio: "",
                       bookValue: "",
                       dividend: "",
                       industryPE: "",
                       epsTTM: "",
                       pcRatio: "",
                       pbRatio: "",
                       dividendYield: "",
                       faceValue: "",
                       deliverables: "",
                       sentiment: "",
                       bse52Low: "",
                       bse52High: ""
                     };

module.exports = {
   getCompanyOverview: function (companyUrl, companyName, industryName) {

     rp({
        url: companyUrl,
        method: 'get',
        timeout: 600000
         })
       .then(function (html){

         //load html contents in jquery selector via cheerio;
         var $ = cheerio.load(html);
         console.log("outside companies list....");
         debugger;
        var consolidated = selectors.COMPANY_PAGE.CONSOLIDATED_OVERVIEW;

        //set industry name for this company
        companyOverviewItem.industry = industryName;

        //get BSE code for this company
        var bseCodeString = $(selectors.COMPANY_PAGE.BSE_CODE).text().split("|")[0].split(":")[1].trim();
        if(bseCodeString.length > 1){
          if(/^[0-9]+$/.test(bseCodeString)){
              companyOverviewItem.bseCode = bseCodeString
          }
          else {
            console.error('BSE Code for the company (' + companyName +') contains non-digits -' + bseCodeString[1]);
          }
        }
        else {
          console.error('BSE code did not parse properly - ' + bseCodeString);
        }

        companyOverviewItem.marketCap = $(consolidated.MCAP).text().replace(',','').trim();
        companyOverviewItem.peRatio = $(consolidated.PE).text().replace(',','').trim();
        companyOverviewItem.bookValue = $(consolidated.BOOK_VALUE).text().replace(',','').trim();
        companyOverviewItem.dividend = $(consolidated.DIVIDEND).text().replace('%','').trim();
        companyOverviewItem.industryPE = $(consolidated.INDUSTRY_PE).text().replace(',','').trim();
        companyOverviewItem.epsTTM = $(consolidated.EPS_TTM).text().replace(',','').trim();
        companyOverviewItem.pcRatio = $(consolidated.PC).text().replace(',','').trim();
        companyOverviewItem.pbRatio = $(consolidated.PB).text().replace(',','').trim();
        companyOverviewItem.dividendYield = $(consolidated.DIV_YIELD).text().replace('%','').trim();
        companyOverviewItem.faceValue = $(consolidated.FV).text().replace(',','').trim();
        companyOverviewItem.deliverables = $(consolidated.DELIVERABLES).first().text().trim();
        //companyOverviewItem.sentiment = ;
        companyOverviewItem.bse52Low = $(selectors.COMPANY_PAGE.BSE_52_LOW).text().replace(',','').trim();
        companyOverviewItem.bse52High = $(selectors.COMPANY_PAGE.BSE_52_HIGH).text().replace(',','').trim();
        debugger;
        //check if the bse code is parsed properly for this company
       if(companyOverviewItem.bseCode != "") {

         /*fs.writeFile('overview/'+ companyName +'.json', JSON.stringify(companyOverviewItem, null, 4), function(err){

             console.log('Company Overview File successfully written! - Check your --Overview-- directory for the .json file');

         })*/

         firebaseSetup.writeCompanyOverviewDataInFirebase(companyOverviewItem);
         console.log('Overview details for company :: ' + companyName + ' :: written successfully');
       }
       else {
         console.error('BSE Code for the company ( ' + companyName + '). Please check the bse code selector and try again!!');
       }
       })
       .catch(function (err) {
       console.error('Error occured while requesting url - ' + companyUrl);
       console.error(err);
       return;
     });

  }
}
