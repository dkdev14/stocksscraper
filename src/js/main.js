var express = require('express');
var cheerio = require('cheerio');
var fs = require('fs');
var request = require('request');
var rp = require('request-promise');
var firebaseSetup = require('./firebaseSetup');
var selectors = require('../resources/selectors');
var urls = require('../resources/urls');
var industryInstance = require('./getIndustryList');

var app = express();
var industryList = [];

app.get('/start', function(req, res) {

  res.setTimeout(600000, function(){
        console.log('Request has timed out.');
            res.send(408);
        });

  //initialize firebase for this app
  firebaseSetup.initializeFirebase();

  //url to navigate to
  var url = urls.BASE_URL + urls.INDUSTRY_CLASSIFICATION_RELATIVE_URL;
  industryInstance.getIndustryList(url);
});

app.listen(8087);
console.log('Server is listening to port 8087');



exports = module.exports = app;
