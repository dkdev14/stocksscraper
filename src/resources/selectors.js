//this file contains various paths and selectors to use to scrape data
module.exports = {
  INDUSTRY_CLASSIFICATION: {
    INDUSTRY_LINKS: 'ul.stat_lflist li',
    INDUSTRY_NAME: 'a',
    COMPANY_LIST: 'div.bsr_table.hist_tbl_hm.PR.Ohidden table tbody tr',
    COMPANY_NAME: 'td.PR span a',
    BSE_CODE: 'td.PR span.ic_tradenwicn.btn_tradep_pop.ML2'
    },
  COMPANY_PAGE: {
    BSE_CODE: '#nChrtPrc > div.PB10 > div.FL.gry10',
    BSE_52_LOW: '#b_52low',
    BSE_52_HIGH: '#b_52high',
    CONSOLIDATED_OVERVIEW: {
      MCAP: '#mktdet_2 > div:nth-child(1) > div:nth-child(1) > div.FR.gD_12',
      PE: '#mktdet_2 > div:nth-child(1) > div:nth-child(2) > div.FR.gD_12',
      BOOK_VALUE: '#mktdet_2 > div:nth-child(1) > div:nth-child(3) > div.FR.gD_12',
      DIVIDEND: '#mktdet_2 > div:nth-child(1) > div:nth-child(4) > div.FR.gD_12',
      INDUSTRY_PE: '#mktdet_2 > div:nth-child(1) > div:nth-child(6) > div.FR.gD_12',
      EPS_TTM: '#mktdet_2 > div:nth-child(2) > div:nth-child(1) > div.FR.gD_12',
      PC: '#mktdet_2 > div:nth-child(2) > div:nth-child(2) > div.FR.gD_12',
      PB: '#mktdet_2 > div:nth-child(2) > div:nth-child(3) > div.FR.gD_12',
      DIV_YIELD: '#mktdet_2 > div:nth-child(2) > div:nth-child(4) > div.FR.gD_12',
      FV: '#mktdet_2 > div:nth-child(2) > div:nth-child(5) > div.FR.gD_12',
      DELIVERABLES: '#tt07 > a'
    }
  }
}
